from puper import Puper
import json

with open('./filestorage/input.json', 'r') as file:
    ds = json.load(file)

out = [Puper.doit(e) for e in ds]

with open('./filestorage/output.json', 'w') as file:
    json.dump(out, file)
